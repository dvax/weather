package love.svyatos.weather.ui.fragments;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.arellomobile.mvp.MvpAppCompatFragment;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import love.svyatos.weather.R;
import love.svyatos.weather.mvp.models.Weather;
import love.svyatos.weather.mvp.presenters.ListPresenter;
import love.svyatos.weather.mvp.views.ListView;
import love.svyatos.weather.ui.adapter.WeatherListAdapter;

/**
 * Author: Svyatoslav Pavlov, Created 03.09.17.
 */

public class ListFragment extends MvpAppCompatFragment implements ListView, SwipeRefreshLayout.OnRefreshListener {

    private static final String ARG_CITY = "ARG_CITY";

    @InjectPresenter
    ListPresenter listPresenter;

    @BindView(R.id.list)
    RecyclerView recyclerView;

    @BindView(R.id.refresh_layout)
    SwipeRefreshLayout refreshLayout;

    private WeatherListAdapter adapter;


    @ProvidePresenter
    ListPresenter providePresenter() {
        return new ListPresenter(getArguments().getLong(ARG_CITY));
    }

    public ListFragment() {
    }

    public static ListFragment newInstance(long cityId) {
        ListFragment fragment = new ListFragment();
        Bundle args = new Bundle();
        args.putLong(ARG_CITY, cityId);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_list, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);

        refreshLayout.setEnabled(true);
        refreshLayout.setRefreshing(true);
        refreshLayout.setColorSchemeColors(Color.RED);
        refreshLayout.setOnRefreshListener(this);

    }

    @Override
    public void onRefresh() {
        listPresenter.onRefresh();
    }

    @Override
    public void setWeather(List<Weather> weatherList, long cityId) {

        refreshLayout.setRefreshing(false);

        if (adapter == null) {
            adapter = new WeatherListAdapter(weatherList, getMvpDelegate(), cityId);
            recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
            recyclerView.setAdapter(adapter);
        } else {
            adapter.swap(weatherList);
        }

    }

    @Override
    public void setState(Parcelable state) {
        recyclerView.getLayoutManager().onRestoreInstanceState(state);
    }


    @Override
    public void onPause() {
        super.onPause();
        if (recyclerView.getLayoutManager() != null) {
            listPresenter.setScrollPosition(recyclerView.getLayoutManager().onSaveInstanceState());
        }
        if (refreshLayout != null) {
            refreshLayout.setRefreshing(false);
            refreshLayout.destroyDrawingCache();
            refreshLayout.clearAnimation();
        }
    }
}
