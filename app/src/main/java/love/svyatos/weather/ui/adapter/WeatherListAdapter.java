package love.svyatos.weather.ui.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.arellomobile.mvp.MvpDelegate;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.bumptech.glide.RequestManager;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import love.svyatos.weather.R;
import love.svyatos.weather.app.WeatherApp;
import love.svyatos.weather.mvp.models.Weather;
import love.svyatos.weather.mvp.presenters.ListAdapterPresenter;
import love.svyatos.weather.mvp.views.ListAdapterView;

/**
 * Author: Svyatoslav Pavlov, Created 03.09.17.
 */

public class WeatherListAdapter extends MvpBaseAdapter<WeatherListAdapter.ViewHolder>
        implements ListAdapterView{

    @Inject
    RequestManager glide;

    @InjectPresenter
    ListAdapterPresenter listAdapterPresenter;


    private final List<Weather> weatherList;

    public WeatherListAdapter(List<Weather> weatherList, MvpDelegate<?> mvpDelegate, long cityId) {
        super(mvpDelegate, String.valueOf(cityId));
        WeatherApp.getAppComponent().inject(this);
        this.weatherList = weatherList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_card, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.bind(weatherList.get(position));
    }

    @Override
    public int getItemCount() {
        return weatherList.size();
    }

    public void swap(List<Weather> weatherList) {
        if (weatherList != null && weatherList.size()>0){
            this.weatherList.clear();
            this.weatherList.addAll(weatherList);
            notifyDataSetChanged();
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.date)
        TextView dateTextView;

        @BindView(R.id.weather)
        TextView weatherTextView;

        @BindView(R.id.icon)
        ImageView iconImageView;

        View view;

        ViewHolder(View itemView) {
            super(itemView);
            view = itemView;
            ButterKnife.bind(this, itemView);
        }

        void bind(Weather weather) {
            dateTextView.setText(weather.getReadableDate());
            weatherTextView.setText(weather.getReadableWeather());
            glide.load(weather.getWeatherIconUrl()).into(iconImageView);

            view.setOnClickListener(v -> listAdapterPresenter.onItemClicked(weather));
        }
    }
}
