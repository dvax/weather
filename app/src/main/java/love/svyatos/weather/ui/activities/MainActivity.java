package love.svyatos.weather.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.SparseArray;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.Spinner;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;

import org.parceler.Parcels;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import love.svyatos.weather.R;
import love.svyatos.weather.app.WeatherApp;
import love.svyatos.weather.common.Const;
import love.svyatos.weather.mvp.models.Weather;
import love.svyatos.weather.mvp.presenters.HomePresenter;
import love.svyatos.weather.mvp.views.HomeView;
import love.svyatos.weather.ui.adapter.MapSpinnerAdapter;
import love.svyatos.weather.ui.fragments.ListFragment;
import ru.terrakok.cicerone.Navigator;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.android.SupportAppNavigator;

public class MainActivity extends MvpAppCompatActivity implements HomeView {

    @Inject
    NavigatorHolder navigatorHolder;

    @InjectPresenter
    HomePresenter homePresenter;

    @BindView(R.id.spinner)
    Spinner spinner;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.refresh)
    Button refreshButton;

    private Navigator navigator = new SupportAppNavigator(this, R.id.container) {
        @Override
        protected Intent createActivityIntent(String screenKey, Object data) {
            switch (screenKey) {
                case Const.NAV_ITEM:
                    Weather weather = (Weather) data;
                    Intent intent = new Intent(MainActivity.this, DetailActivity.class);
                    intent.putExtra(DetailActivity.ARG_WEATHER, Parcels.wrap(weather));
                    return intent;
            }
            return null;
        }

        @Override
        protected Fragment createFragment(String screenKey, Object data) {
            switch (screenKey) {
                case Const.NAV_CITY:
                    return ListFragment.newInstance((Long) data);
            }
            return null;
        }
    };

    public MainActivity() {
        WeatherApp.getAppComponent().inject(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);


        spinner.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                homePresenter.onCitySelected(id);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

    }

    @OnClick(R.id.refresh)
    public void onRefreshClicked(){
        homePresenter.onRefreshClicked();
    }

    @Override
    public void setCities(SparseArray<Weather> cities) {
        spinner.setVisibility(View.VISIBLE);
        spinner.setAdapter(new MapSpinnerAdapter(cities, this));

    }

    @Override
    public void showRefreshButton() {
        refreshButton.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideRefreshButton() {
        refreshButton.setVisibility(View.GONE);
    }

    @Override
    public void hideSpinner() {
        spinner.setVisibility(View.GONE);
    }


    @Override
    protected void onPause() {
        super.onPause();
        navigatorHolder.removeNavigator();
    }

    @Override
    protected void onResume() {
        super.onResume();
        navigatorHolder.setNavigator(navigator);
    }

}
