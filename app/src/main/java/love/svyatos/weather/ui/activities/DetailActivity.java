package love.svyatos.weather.ui.activities;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.arellomobile.mvp.MvpAppCompatActivity;
import com.arellomobile.mvp.presenter.InjectPresenter;
import com.arellomobile.mvp.presenter.ProvidePresenter;
import com.bumptech.glide.RequestManager;

import org.parceler.Parcels;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import love.svyatos.weather.R;
import love.svyatos.weather.app.WeatherApp;
import love.svyatos.weather.mvp.models.Weather;
import love.svyatos.weather.mvp.presenters.DetailPresenter;
import love.svyatos.weather.mvp.views.DetailView;

public class DetailActivity extends MvpAppCompatActivity implements DetailView {

    public static final String ARG_WEATHER = "weather";

    @BindView(R.id.date)
    TextView dateTextView;

    @BindView(R.id.weather)
    TextView weatherTextView;

    @BindView(R.id.icon)
    ImageView iconImageView;

    @BindView(R.id.wind)
    TextView windTextView;

    @BindView(R.id.pressure)
    TextView pressureTextView;

    @Inject
    RequestManager glide;

    @InjectPresenter
    DetailPresenter detailPresenter;

    @ProvidePresenter
    DetailPresenter providePresenter() {
        Weather weather = Parcels.unwrap(getIntent().getParcelableExtra(ARG_WEATHER));
        return new DetailPresenter(weather);
    }

    public DetailActivity() {
        WeatherApp.getAppComponent().inject(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        ButterKnife.bind(this);
    }

    @Override
    public void setMainInfo(String info) {
        weatherTextView.setText(info);
    }

    @Override
    public void setIcon(String iconUrl) {
        glide.load(iconUrl).into(iconImageView);
    }

    @Override
    public void setDate(String date) {
        dateTextView.setText(date);
    }

    @Override
    public void setPressure(Float pressure) {
        pressureTextView.setText(getResources().getString(R.string.pressure, pressure));
    }

    @Override
    public void setWindSpeed(Float speed) {
        windTextView.setText(getResources().getString(R.string.wind, speed));
    }
}
