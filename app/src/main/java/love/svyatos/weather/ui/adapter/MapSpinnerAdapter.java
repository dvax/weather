package love.svyatos.weather.ui.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.ThemedSpinnerAdapter;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import love.svyatos.weather.mvp.models.Weather;

/**
 * Author: Svyatoslav Pavlov, Created 03.09.17.
 */

public class MapSpinnerAdapter extends SparseArrayAdapter<Weather> implements ThemedSpinnerAdapter {

    private final Helper dropDownHelper;

    public MapSpinnerAdapter(SparseArray<Weather> items, Context context) {
        super.setData(items);
        dropDownHelper = new ThemedSpinnerAdapter.Helper(context);
    }

    @Override
    public Resources.Theme getDropDownViewTheme() {
        return dropDownHelper.getDropDownViewTheme();
    }

    @Override
    public void setDropDownViewTheme(Resources.Theme theme) {
        dropDownHelper.setDropDownViewTheme(theme);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View view;

        if (convertView == null) {
            LayoutInflater inflater = dropDownHelper.getDropDownViewInflater();
            view = inflater.inflate(android.R.layout.simple_list_item_1, parent, false);
        } else {
            view = convertView;
        }

        TextView textView = (TextView) view.findViewById(android.R.id.text1);
        textView.setText(getItem(position).getName());
        return view;

    }
}
