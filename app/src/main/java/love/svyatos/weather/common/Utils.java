package love.svyatos.weather.common;

import java.util.Objects;

/**
 * Author: Svyatoslav Pavlov, Created 03.09.17.
 */

public class Utils {


    public static <T> String join(T[] array, String glue) {

        if (array.length == 0) {
            return "";
        }

        StringBuilder result = new StringBuilder();

        result.append(array[0]);

        for (int i = 1; i < array.length; i++) {
            result.append(glue).append(array[i]);
        }

        return result.toString();

    }

}
