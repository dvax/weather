package love.svyatos.weather.common;

/**
 * Author: Svyatoslav Pavlov, Created 03.09.17.
 */

public class Const {
    public static final String NAV_CITY = "NAV_CITY";
    public static final String NAV_ITEM = "NAV_ITEM";
}
