package love.svyatos.weather.di.modules;

import android.content.Context;
import android.support.annotation.NonNull;

import com.bumptech.glide.Glide;
import com.bumptech.glide.RequestManager;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Author: Svyatoslav Pavlov, Created 03.09.17.
 */

@Module(includes = {AppModule.class})
public class ImageModule {


    @Provides
    @NonNull
    @Singleton
    RequestManager provideGlide(Context context){
        return Glide.with(context);
    }

}
