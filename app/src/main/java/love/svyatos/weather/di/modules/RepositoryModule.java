package love.svyatos.weather.di.modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import love.svyatos.weather.mvp.models.repositories.WeatherRepository;

/**
 * Author: Svyatoslav Pavlov, Created 03.09.17.
 */

@Module
public class RepositoryModule {

    @Singleton
    @Provides
    WeatherRepository provideWeatherRepository(){
        return new WeatherRepository();
    }

}
