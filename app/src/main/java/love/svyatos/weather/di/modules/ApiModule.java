package love.svyatos.weather.di.modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import love.svyatos.weather.app.Api;
import retrofit2.Retrofit;

/**
 * Author: Svyatoslav Pavlov, Created 06.08.17.
 */


@Module(includes = {RetrofitModule.class})
public class ApiModule {

    @Provides
    @Singleton
    Api provideApi(Retrofit retrofit) {
        return retrofit.create(Api.class);
    }
}
