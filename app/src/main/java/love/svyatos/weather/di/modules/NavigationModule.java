package love.svyatos.weather.di.modules;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import ru.terrakok.cicerone.Cicerone;
import ru.terrakok.cicerone.NavigatorHolder;
import ru.terrakok.cicerone.Router;

/**
 * Author: Svyatoslav Pavlov, Created 03.09.17.
 */


@Module
public class NavigationModule {

    private Cicerone<Router> mCicerone;

    public NavigationModule() {
        initCicerone();
    }

    private void initCicerone() {
        mCicerone = Cicerone.create(new Router());
    }

    @Provides
    @Singleton
    NavigatorHolder provideHolder() {
        return mCicerone.getNavigatorHolder();
    }

    @Provides
    @Singleton
    Router getRouter() {
        return mCicerone.getRouter();
    }

}

