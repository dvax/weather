package love.svyatos.weather.di;

import javax.inject.Singleton;

import dagger.Component;
import love.svyatos.weather.di.modules.ApiModule;
import love.svyatos.weather.di.modules.AppModule;
import love.svyatos.weather.di.modules.ImageModule;
import love.svyatos.weather.di.modules.NavigationModule;
import love.svyatos.weather.di.modules.RepositoryModule;
import love.svyatos.weather.di.modules.RetrofitModule;
import love.svyatos.weather.mvp.models.repositories.WeatherRepository;
import love.svyatos.weather.mvp.presenters.HomePresenter;
import love.svyatos.weather.mvp.presenters.ListAdapterPresenter;
import love.svyatos.weather.mvp.presenters.ListPresenter;
import love.svyatos.weather.ui.activities.DetailActivity;
import love.svyatos.weather.ui.activities.MainActivity;
import love.svyatos.weather.ui.adapter.WeatherListAdapter;

/**
 * Author: Svyatoslav Pavlov, Created 03.09.17.
 */

@Singleton
@Component(modules = {
        ApiModule.class,
        AppModule.class,
        RetrofitModule.class,
        NavigationModule.class,
        ImageModule.class,
        RepositoryModule.class})
public interface AppComponent {

    void inject(WeatherRepository weatherRepository);

    void inject(HomePresenter homePresenter);

    void inject(MainActivity mainActivity);

    void inject(ListPresenter listPresenter);

    void inject(WeatherListAdapter weatherListAdapter);

    void inject(ListAdapterPresenter listAdapterPresenter);

    void inject(DetailActivity detailActivity);
}
