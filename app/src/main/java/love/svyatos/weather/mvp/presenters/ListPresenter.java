package love.svyatos.weather.mvp.presenters;

import android.os.Parcelable;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.observers.DefaultObserver;
import love.svyatos.weather.app.WeatherApp;
import love.svyatos.weather.mvp.models.Weather;
import love.svyatos.weather.mvp.models.repositories.WeatherRepository;
import love.svyatos.weather.mvp.views.ListView;
import ru.terrakok.cicerone.Router;

/**
 * Author: Svyatoslav Pavlov, Created 03.09.17.
 */

@InjectViewState
public class ListPresenter extends MvpPresenter<ListView> {


    @Inject
    WeatherRepository weatherRepository;

    @Inject
    Router router;

    private long cityId;

    public ListPresenter(long cityId) {

        WeatherApp.getAppComponent().inject(this);

        this.cityId = cityId;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        showForecast();

    }

    public void onRefresh() {
        showForecast();
    }

    public void setScrollPosition(Parcelable scrollPosition) {
        getViewState().setState(scrollPosition);
    }

    private void showForecast() {

        weatherRepository.getForecast(cityId)
                .subscribe(new DefaultObserver<List<Weather>>() {
                    @Override
                    public void onNext(@NonNull List<Weather> weatherList) {
                        getViewState().setWeather(weatherList, cityId);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        e.printStackTrace();
                        router.showSystemMessage("Error");
                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }

}
