package love.svyatos.weather.mvp.models.repositories;

import android.util.SparseArray;

import java.util.List;

import javax.inject.Inject;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import love.svyatos.weather.app.Api;
import love.svyatos.weather.app.WeatherApp;
import love.svyatos.weather.common.Utils;
import love.svyatos.weather.mvp.models.Weather;
import love.svyatos.weather.mvp.models.gson.WeatherGroup;

/**
 * Author: Svyatoslav Pavlov, Created 03.09.17.
 */

public class WeatherRepository {

    @Inject
    Api api;

    private Long[] cityIds;

    public WeatherRepository() {
        WeatherApp.getAppComponent().inject(this);
    }

    public WeatherRepository(Api api){
        this.api = api;
    }

    public Observable<SparseArray<Weather>> getGroup(Long[] cityIds) {

        this.cityIds = cityIds;

        String ids = Utils.join(cityIds, ",");

        return api.getGroup(ids, getUnits())
                .subscribeOn(Schedulers.io())
                .map(WeatherGroup::getList)
                .map(weathers -> {
                    SparseArray<Weather> weatherArray = new SparseArray<>();

                    for (Weather weather : weathers) {
                        weatherArray.put(weather.getCityId(), weather);
                    }

                    return weatherArray;
                })
                .observeOn(AndroidSchedulers.mainThread());


    }

    public Observable<List<Weather>> getForecast(Long id) {

        return api.getForecast(id, getUnits())
                .subscribeOn(Schedulers.io())
                .map(WeatherGroup::getList)
                .onErrorResumeNext(
                        api.getGroup(Utils.join(cityIds, ","), getUnits())
                                .map(WeatherGroup::getList)
                                .flatMap(Observable::fromIterable)
                                .filter(weather -> Long.valueOf(weather.getCityId()).equals(id))
                                .toList()
                                .toObservable()
                )
                .observeOn(AndroidSchedulers.mainThread());

    }

    private String getUnits() {
        return "metric";
    }

}
