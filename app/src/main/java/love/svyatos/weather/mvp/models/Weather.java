package love.svyatos.weather.mvp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Author: Svyatoslav Pavlov, Created 03.09.17.
 */

@Parcel
public class Weather {

    @SerializedName("weather")
    @Expose
    List<Precipitation> precipitations = null;
    @SerializedName("main")
    @Expose
    Details details;
    @SerializedName("visibility")
    @Expose
    Integer visibility;
    @SerializedName("wind")
    @Expose
    Wind wind;

    @SerializedName("clouds")
    @Expose
    Clouds clouds;

    @SerializedName("dt")
    @Expose
    Integer dt;

    @SerializedName("id")
    @Expose
    Integer cityId;

    @SerializedName("name")
    @Expose
    String name;


    public Integer getVisibility() {
        return visibility;
    }

    public Float getWind() {
        return wind.getSpeed();
    }

    public Float getClouds() {
        return clouds.getAll();
    }

    public Float getPressure(){
        return details.getPressure();
    }

    private Long getDate() {
        return dt * 1000L;
    }

    public String getReadableDate() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM HH:mm", Locale.getDefault());
        return simpleDateFormat.format(new Date(getDate()));
    }

    public Integer getCityId() {
        return cityId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReadableWeather() {
        return String.format(Locale.getDefault(), "%+.0f, %s",
                details.getTemp(), precipitations.get(0).description);
    }


    public String getWeatherIconUrl() {
        return String.format(Locale.getDefault(), "http://openweathermap.org/img/w/%s.png",
                precipitations.get(0).getIcon());
    }


    @Parcel
    static class Precipitation {

        @SerializedName("id")
        @Expose
        Integer id;

        @SerializedName("main")
        @Expose
        String main;

        @SerializedName("description")
        @Expose
        String description;

        @SerializedName("icon")
        @Expose
        String icon;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getMain() {
            return main;
        }

        public void setMain(String main) {
            this.main = main;
        }

        public String getDescription() {
            return description;
        }

        public void setDescription(String description) {
            this.description = description;
        }

        public String getIcon() {
            return icon;
        }

        public void setIcon(String icon) {
            this.icon = icon;
        }

    }

    @Parcel
    static class Details {


        @SerializedName("temp")
        @Expose
        Float temp;
        @SerializedName("pressure")
        @Expose
        Float pressure;
        @SerializedName("humidity")
        @Expose
        Float humidity;
        @SerializedName("temp_min")
        @Expose
        Float tempMin;
        @SerializedName("temp_max")
        @Expose
        Float tempMax;

        public Float getTemp() {
            return temp;
        }

        public Float getPressure() {
            return pressure;
        }

        public Float getHumidity() {
            return humidity;
        }


        public Float getTempMin() {
            return tempMin;
        }

        public Float getTempMax() {
            return tempMax;
        }

    }

    @Parcel
    static class Wind {

        @SerializedName("speed")
        @Expose
        Float speed;

        public Float getSpeed() {
            return speed;
        }
    }

    @Parcel
    static class Clouds{

        @SerializedName("all")
        @Expose
        Float all;

        public Float getAll() {
            return all;
        }
    }
}
