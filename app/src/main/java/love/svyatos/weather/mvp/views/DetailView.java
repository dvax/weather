package love.svyatos.weather.mvp.views;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

/**
 * @author Svyatoslav Pavlov
 */

@StateStrategyType(AddToEndSingleStrategy.class)
public interface DetailView extends MvpView {

    void setMainInfo(String info);

    void setIcon(String iconUrl);

    void setDate(String date);

    void setPressure(Float pressure);

    void setWindSpeed(Float speed);

}
