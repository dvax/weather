package love.svyatos.weather.mvp.presenters;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import javax.inject.Inject;

import love.svyatos.weather.app.WeatherApp;
import love.svyatos.weather.common.Const;
import love.svyatos.weather.mvp.models.Weather;
import love.svyatos.weather.mvp.views.ListAdapterView;
import ru.terrakok.cicerone.Router;

/**
 * Author: Svyatoslav Pavlov, Created 03.09.17.
 */

@InjectViewState
public class ListAdapterPresenter extends MvpPresenter<ListAdapterView> {

    @Inject
    Router router;

    public ListAdapterPresenter() {
        WeatherApp.getAppComponent().inject(this);
    }

    public void onItemClicked(Weather weather){
        router.navigateTo(Const.NAV_ITEM, weather);
    }
}
