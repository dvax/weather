package love.svyatos.weather.mvp.models.gson;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import love.svyatos.weather.mvp.models.Weather;

/**
 * Author: Svyatoslav Pavlov, Created 03.09.17.
 */

public class WeatherGroup {


    @SerializedName("cnt")
    @Expose
    private Integer count;

    @SerializedName("list")
    @Expose
    private List<Weather> list = null;

    public Integer getCount() {
        return count;
    }

    public List<Weather> getList() {
        return list;
    }

}
