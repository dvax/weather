package love.svyatos.weather.mvp.presenters;

import android.util.SparseArray;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import javax.inject.Inject;

import io.reactivex.annotations.NonNull;
import io.reactivex.observers.DefaultObserver;
import love.svyatos.weather.app.WeatherApp;
import love.svyatos.weather.common.Const;
import love.svyatos.weather.common.Defaults;
import love.svyatos.weather.mvp.models.Weather;
import love.svyatos.weather.mvp.models.repositories.WeatherRepository;
import love.svyatos.weather.mvp.views.HomeView;
import ru.terrakok.cicerone.Router;

/**
 * Author: Svyatoslav Pavlov, Created 03.09.17.
 */

@InjectViewState
public class HomePresenter extends MvpPresenter<HomeView> {

    @Inject
    Router router;

    @Inject
    WeatherRepository repository;

    private long currentCity = -1;

    public HomePresenter() {
        WeatherApp.getAppComponent().inject(this);
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();
        loadCities();

    }

    public void onCitySelected(long id) {
        if (currentCity != id) {
            router.newRootScreen(Const.NAV_CITY, id);
            currentCity = id;
        }
    }

    public void onRefreshClicked() {
        loadCities();
    }

    private void loadCities() {

        repository.getGroup(Defaults.getCityIds())
                .subscribe(new DefaultObserver<SparseArray<Weather>>() {
                    @Override
                    public void onNext(@NonNull SparseArray<Weather> weatherLongSparseArray) {
                        getViewState().hideRefreshButton();
                        getViewState().setCities(weatherLongSparseArray);
                    }

                    @Override
                    public void onError(@NonNull Throwable e) {
                        getViewState().showRefreshButton();
                        getViewState().hideSpinner();
                        e.printStackTrace();
                    }

                    @Override
                    public void onComplete() {

                    }
                });

    }
}
