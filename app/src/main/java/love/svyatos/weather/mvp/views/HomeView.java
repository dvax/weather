package love.svyatos.weather.mvp.views;

import android.util.SparseArray;

import com.arellomobile.mvp.MvpView;
import com.arellomobile.mvp.viewstate.strategy.AddToEndSingleStrategy;
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType;

import love.svyatos.weather.mvp.models.Weather;

/**
 * Author: Svyatoslav Pavlov, Created 03.09.17.
 */

@StateStrategyType(AddToEndSingleStrategy.class)
public interface HomeView extends MvpView {

    void setCities(SparseArray<Weather> cities);

    void showRefreshButton();

    void hideRefreshButton();

    void hideSpinner();


}
