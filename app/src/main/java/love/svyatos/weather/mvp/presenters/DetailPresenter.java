package love.svyatos.weather.mvp.presenters;

import com.arellomobile.mvp.InjectViewState;
import com.arellomobile.mvp.MvpPresenter;

import love.svyatos.weather.mvp.models.Weather;
import love.svyatos.weather.mvp.views.DetailView;

/**
 * @author Svyatoslav Pavlov
 */

@InjectViewState
public class DetailPresenter extends MvpPresenter<DetailView> {

    private Weather weather;

    public DetailPresenter(Weather weather) {
        this.weather = weather;
    }

    @Override
    protected void onFirstViewAttach() {
        super.onFirstViewAttach();

        getViewState().setDate(weather.getReadableDate());
        getViewState().setIcon(weather.getWeatherIconUrl());
        getViewState().setMainInfo(weather.getReadableWeather());
        getViewState().setPressure(weather.getPressure());
        getViewState().setWindSpeed(weather.getWind());
    }
}
