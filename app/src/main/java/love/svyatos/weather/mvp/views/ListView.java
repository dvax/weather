package love.svyatos.weather.mvp.views;

import android.os.Parcelable;

import com.arellomobile.mvp.MvpView;

import java.util.List;

import love.svyatos.weather.mvp.models.Weather;

/**
 * Author: Svyatoslav Pavlov, Created 03.09.17.
 */

public interface ListView extends MvpView {

    void setWeather(List<Weather> weatherList, long cityId);

    void setState(Parcelable state);
}
