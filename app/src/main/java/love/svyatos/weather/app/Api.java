package love.svyatos.weather.app;

import io.reactivex.Observable;
import love.svyatos.weather.mvp.models.gson.WeatherGroup;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Author: Svyatoslav Pavlov, Created 03.09.17.
 */

public interface Api {

    @GET("group")
    Observable<WeatherGroup> getGroup(@Query("id") String ids, @Query("units") String units);

    @GET("forecast")
    Observable<WeatherGroup> getForecast(@Query("id") long id, @Query("units") String units);

}
