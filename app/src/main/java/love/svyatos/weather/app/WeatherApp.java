package love.svyatos.weather.app;

import android.app.Application;

import love.svyatos.weather.di.AppComponent;
import love.svyatos.weather.di.DaggerAppComponent;
import love.svyatos.weather.di.modules.AppModule;

/**
 * Author: Svyatoslav Pavlov, Created 03.09.17.
 */

public class WeatherApp extends Application {

    private static AppComponent appComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        appComponent = DaggerAppComponent.builder()
                .appModule(new AppModule(this))
                .build();
    }

    public static AppComponent getAppComponent() {
        return appComponent;
    }
}
