package love.svyatos.weather;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import love.svyatos.weather.app.Api;
import love.svyatos.weather.common.Defaults;
import love.svyatos.weather.mvp.models.repositories.WeatherRepository;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * @author Svyatoslav Pavlov
 */

@RunWith(MockitoJUnitRunner.class)
public class DataTest {


    @Mock
    Api api;

    @InjectMocks
    private WeatherRepository repository;

    @Before
    public void init(){
        initMocks(this);
    }

    @Test
    public void dataGroup() throws Exception {


        try {
            repository.getGroup(Defaults.getCityIds()).subscribe();
        }
        catch (NullPointerException ignored){
        }

        verify(api).getGroup(any(String.class), eq("metric"));

    }

    @Test
    public void dataForecast() throws Exception {


        try {
            repository.getForecast(Defaults.getCityIds()[0]).subscribe();
        }
        catch (NullPointerException ignored){
        }

        verify(api).getForecast(any(Long.class), eq("metric"));

    }

}

